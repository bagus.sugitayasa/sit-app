import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { RespData } from '../model/RespData';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedInStatus = false //JSON.parse(localStorage.getItem('loggedIn') || 'false')
  public isAuthenticated = new BehaviorSubject<boolean>(false);

  constructor(
    private http: HttpClient,
    private router: Router) { }

  setLoggedIn(value: boolean) {
    this.loggedInStatus = value
    this.isAuthenticated.next(value)
    //localStorage.setItem('loggedIn', 'true')
  }

  get isLoggedIn() {
    return this.loggedInStatus //JSON.parse(localStorage.getItem('loggedIn') || this.loggedInStatus.toString())
  }

  getUserDetails(username: string, password: string) {
    return this.http.post<RespData>('/api/auth.php', {
      username,
      password
    })
  }

  logOut(redirect: string) {
    try {
      this.loggedInStatus = false
      this.isAuthenticated.next(false)
      this.router.navigate([redirect])
    } catch (error) {
      console.log(error)
    }
  }
}
