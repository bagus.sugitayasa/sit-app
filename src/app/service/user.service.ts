import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { isLoggedIn, RespData } from '../model/RespData';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  doGetSomeData() {
    return this.http.get<RespData>('api/database.php')
  }

  isLoggedIn() : Observable<isLoggedIn> {
    return this.http.get<isLoggedIn>('api/isLoggedIn.php')
  }
}
