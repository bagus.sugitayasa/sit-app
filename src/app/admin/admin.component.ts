import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  message = "Loading..."

  constructor(private user: UserService) { }

  ngOnInit(): void {
    this.user.doGetSomeData().subscribe(data => {
      this.message = data.message
    })
  }

}
