export interface RespData {
    success : boolean, 
    message : string
}

export interface isLoggedIn {
    status : boolean
}