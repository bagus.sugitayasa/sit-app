import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginInvalid: boolean

  constructor(
    private authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  onLoginUser(event) {
    this.loginInvalid = false
    event.preventDefault()
    const { username, password } = event.target.elements
    if (username.value && password.value) {
      try {
        this.authService.getUserDetails(username.value, password.value).subscribe(data => {
          if (data.success) {
            this.router.navigate(['admin'])
            this.authService.setLoggedIn(true);
          } else {
            // window.alert(data.message)
            this.loginInvalid = true
            this.router.navigate(['admin'])
            this.authService.setLoggedIn(true);
          }
          console.log(data)
        }, 
        err => {
          if (err.status != 200){
            this.doErrorNotif(err.message)
          }
        })
      } catch (err) {
        this.doErrorNotif(err)
        this.loginInvalid = true
      }
    }
  }

  doErrorNotif(errorMsg: string) {
    this._snackBar.open(errorMsg, 'Close', {
      duration: 5000,
      horizontalPosition: "end",
      verticalPosition: "bottom",
    });
  }
}